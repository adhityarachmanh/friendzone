/*
module  : WELCOME CONTROLLER
creator : arh
os      : linux-gnu
created : Rab Jan 13 13:36:06 WIB 2021
product : ARH
version : v1.0
*/

part of '../app.dart';

class WelcomeController with ChangeNotifier {
    final String title = "WelcomeControllerWorks!";
   
    void onInit(BuildContext context){
    	print("Init WelcomeController");
    }
}
