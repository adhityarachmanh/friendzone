/*
module  : DASHBOARD SCREEN
creator : arh
os      : linux-gnu
created : Rab Jan 13 13:35:44 WIB 2021
product : ARH
version : v1.0
*/

part of '../app.dart';

class DashboardScreen extends StatefulWidget {
  static final routeName = "/DashboardScreen";

  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  final TrackingScrollController _trackingScrollController = TrackingScrollController();

  @override
  void dispose() {
    _trackingScrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ResponsiveWidget(
          mobile:_DashboardScreenMobile(scrollController: _trackingScrollController),
        ));
  }
}

class _DashboardScreenMobile extends StatelessWidget {
  final TrackingScrollController scrollController;

  const _DashboardScreenMobile({Key key, @required this.scrollController})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    // Global controller
    final globalState = Provider.of<IndexController>(context);
    final globalDispatch = Provider.of<IndexController>(context, listen: false);
    // Route
    final route = Provider.of<RouteFunction>(context, listen: false);
    final routeParams = route.getParams(context);
    // Websocket
    final websocket = Provider.of<WebsocketFunction>(context, listen: false);
    // Local controller
    final state = Provider.of<DashboardController>(context);
    final dispatch = Provider.of<DashboardController>(context, listen: false);
    return CustomScrollView(
      slivers: [
        SliverToBoxAdapter(
          child: Container(
            height: SizeConfig.screenHeight,
            child: Center(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("DashboardMobileScreen!",style: MyText.dark(font: MyFont.montserrat).copyWith(fontSize: 25)),
                SizedBox(height: defaultMargin),
                Text(state.title, style: MyText.dark(font: MyFont.montserrat).copyWith(fontSize: 25)),
              ],
            )),
          ),
        )
      ],
    );
  }
}
