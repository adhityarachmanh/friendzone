/*
module  : WELCOME SCREEN
creator : arh
os      : linux-gnu
created : Rab Jan 13 13:36:06 WIB 2021
product : ARH
version : v1.0
*/

part of '../app.dart';

class WelcomeScreen extends StatefulWidget {
  static final routeName = "/WelcomeScreen";

  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  final TrackingScrollController _trackingScrollController =
      TrackingScrollController();

  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _trackingScrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ResponsiveWidget(
      mobile: _WelcomeScreenMobile(scrollController: _trackingScrollController),
    ));
  }
}

class _WelcomeScreenMobile extends StatelessWidget {
  final TrackingScrollController scrollController;

  const _WelcomeScreenMobile({Key key, @required this.scrollController})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    // Global controller
    final globalState = Provider.of<IndexController>(context);
    final globalDispatch = Provider.of<IndexController>(context, listen: false);
    // Route
    final route = Provider.of<RouteFunction>(context, listen: false);
    final routeParams = route.getParams(context);
    // Websocket
    final websocket = Provider.of<WebsocketFunction>(context, listen: false);
    // Local controller
    final state = Provider.of<WelcomeController>(context);
    final dispatch = Provider.of<WelcomeController>(context, listen: false);

    globalDispatch.setStatusBar(background: Palette.fzColor);

    return CustomScrollView(
      slivers: [
        SliverToBoxAdapter(
          child: Container(
            height: SizeConfig.screenHeight * 0.55,
            // decoration: BoxDecoration(color: Palette.primaryColor),
            child: Stack(
              children: [
                Container(
                  height: double.infinity,
                  child: Center(
                    child: Text(
                      "Image",
                      style: MyText.dark(font: MyFont.montserrat),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
        SliverToBoxAdapter(
          child: Container(
            margin: EdgeInsets.symmetric(
              horizontal: defaultMargin*1.2,
            ),
            child: Center(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ButtonRoundedWidget(
                  color: Palette.primaryColor,
                  label: "Sign Up free",
                  onPressed: () =>
                      route.navigateTo(SignupemailScreen.routeName),
                ),
                ButtonOutlinedWidget(
                  icon: Icon(Icons.phone_android_outlined),
                  borderColor: Palette.grayColor,
                  label: "Continue with phone number",
                  onPressed: () {},
                ),
                ButtonOutlinedWidget(
                  icon: ImageContainerWidget(
                    "assets/logos/google.png",
                    width: 24,
                    height: 24,
                  ),
                  borderColor: Palette.grayColor,
                  label: "Continue with Google",
                  onPressed: () {},
                ),
                ButtonOutlinedWidget(
                  icon: ImageContainerWidget(
                    "assets/logos/facebook.png",
                    height: 24,
                    width: 24,
                  ),
                  borderColor: Palette.grayColor,
                  label: "Continue with Facebook",
                  onPressed: () {},
                ),
                ButtonFlatWidget(
                  label: "Log in",
                  onTap: () => route.navigateTo(SigninScreen.routeName),
                )
              ],
            )),
          ),
        )
      ],
    );
  }
}
