/*
module  : SPLASH SCREEN
creator : arh
os      : linux-gnu
created : Rab Jan 13 13:32:20 WIB 2021
product : ARH
version : v1.0
*/

part of '../app.dart';

class SplashScreen extends StatefulWidget {
  static final routeName = "/SplashScreen";

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final TrackingScrollController _trackingScrollController =
      TrackingScrollController();

  @override
  void dispose() {
    _trackingScrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Palette.fzColor,
        body: ResponsiveWidget(
          mobile:
              _SplashScreenMobile(scrollController: _trackingScrollController),
        ));
  }
}

class _SplashScreenMobile extends StatelessWidget {
  final TrackingScrollController scrollController;

  const _SplashScreenMobile({Key key, @required this.scrollController})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    // Global controller
    final globalState = Provider.of<IndexController>(context);
    final globalDispatch = Provider.of<IndexController>(context, listen: false);
    // Route
    final route = Provider.of<RouteFunction>(context, listen: false);
    final routeParams = route.getParams(context);
    // Websocket
    final websocket = Provider.of<WebsocketFunction>(context, listen: false);
    // Local controller
    final state = Provider.of<SplashController>(context);
    final dispatch = Provider.of<SplashController>(context, listen: false);

    globalDispatch.setStatusBar(background: Palette.fzColor);
    return CustomScrollView(
      slivers: [
        SliverToBoxAdapter(
          child: Container(
            height: SizeConfig.screenHeight,
            child: Center(
                child: Text("FriendZone",
                    style: MyText.light(font: MyFont.montserrat)
                        .copyWith(fontSize: 25, fontWeight: FontWeight.bold))),
          ),
        )
      ],
    );
  }
}
