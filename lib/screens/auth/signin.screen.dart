/*
module  : SIGNIN SCREEN
creator : arh
os      : linux-gnu
created : Rab Jan 13 13:49:44 WIB 2021
product : ARH
version : v1.0
*/

part of '../../app.dart';

class SigninScreen extends StatefulWidget {
  static final routeName = "/SigninScreen";

  @override
  _SigninScreenState createState() => _SigninScreenState();
}

class _SigninScreenState extends State<SigninScreen> {
  final TrackingScrollController _trackingScrollController =
      TrackingScrollController();

  @override
  void dispose() {
    _trackingScrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // Route
    final route = Provider.of<RouteFunction>(context, listen: false);
    return WillPopScope(
      onWillPop: () => route.navigateToAndRemoveUntil(WelcomeScreen.routeName),
      child: Scaffold(
          body: ResponsiveWidget(
        mobile:
            _SigninScreenMobile(scrollController: _trackingScrollController),
      )),
    );
  }
}

class _SigninScreenMobile extends StatelessWidget {
  final TrackingScrollController scrollController;

  const _SigninScreenMobile({Key key, @required this.scrollController})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    // Global controller
    final globalState = Provider.of<IndexController>(context);
    final globalDispatch = Provider.of<IndexController>(context, listen: false);
    // Route
    final route = Provider.of<RouteFunction>(context, listen: false);
    final routeParams = route.getParams(context);
    // Websocket
    final websocket = Provider.of<WebsocketFunction>(context, listen: false);
    // Local controller
    final state = Provider.of<SigninController>(context);
    final dispatch = Provider.of<SigninController>(context, listen: false);
    globalDispatch.setStatusBar(background: Palette.fzColor);
    return CustomScrollView(
      slivers: [
        SliverToBoxAdapter(
          child: Container(
            height: SizeConfig.screenHeight,
            child: Center(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("SigninMobileScreen!",
                    style: MyText.primary(font: MyFont.montserrat)
                        .copyWith(fontSize: 25)),
                SizedBox(height: defaultMargin),
                Text(state.title,
                    style: MyText.primary(font: MyFont.montserrat)
                        .copyWith(fontSize: 25)),
              ],
            )),
          ),
        )
      ],
    );
  }
}
