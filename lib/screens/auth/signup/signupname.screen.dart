/*
module  : SIGNUPNAME SCREEN
creator : adhityarachmanh
os      : darwin20
created : Sun Jan 17 00:02:16 WIB 2021
product : ARH
version : v1.0
*/

part of '../../../app.dart';

class SignupnameScreen extends StatefulWidget {
  static final routeName = "/SignupnameScreen";

  @override
  _SignupnameScreenState createState() => _SignupnameScreenState();
}

class _SignupnameScreenState extends State<SignupnameScreen> {
  final TrackingScrollController _trackingScrollController = TrackingScrollController();

  @override
  void dispose() {
    _trackingScrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
    body: ResponsiveWidget(
      mobile:_SignupnameScreenMobile(scrollController: _trackingScrollController),
    ));
  }
}

class _SignupnameScreenMobile extends StatelessWidget {
  final TrackingScrollController scrollController;

  const _SignupnameScreenMobile({Key key, @required this.scrollController})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    // Global controller
    final globalState = Provider.of<IndexController>(context);
    final globalDispatch = Provider.of<IndexController>(context, listen: false);
    // Route
    final route = Provider.of<RouteFunction>(context, listen: false);
    final routeParams = route.getParams(context);
    // Websocket
    final websocket = Provider.of<WebsocketFunction>(context, listen: false);
    // Local controller
    final state = Provider.of<SignupnameController>(context);
    final dispatch = Provider.of<SignupnameController>(context, listen: false);
    globalDispatch.setStatusBar(background: Palette.fzColor);
    return CustomScrollView(
      slivers: [
        SliverToBoxAdapter(
          child: Container(
            height: SizeConfig.screenHeight,
            child: Center(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("SignupnameMobileScreen!",style: MyText.dark(font: MyFont.montserrat).copyWith(fontSize: 25)),
                SizedBox(height: defaultMargin),
                Text(state.title, style: MyText.dark(font: MyFont.montserrat).copyWith(fontSize: 25)),
              ],
            )),
          ),
        )
      ],
    );
  }
}
