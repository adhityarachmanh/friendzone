/*
module  : SIGNUPPASSWORD CONTROLLER
creator : adhityarachmanh
os      : darwin20
created : Sat Jan 16 23:40:44 WIB 2021
product : ARH
version : v1.0
*/

part of '../../../app.dart';

class SignuppasswordController with ChangeNotifier {
    final String title = "SignuppasswordControllerWorks!";
    String _password = "";
    String get password => _password;
    Color get isActiveColor =>
       password.length == 8? Palette.fzColor : Palette.grayColor;

    void setPassword(String password) {
      _password = password;
      notifyListeners();
  }
    void onInit(BuildContext context){
    	print("Init SignuppasswordController");
    }
    void check(
      {@required BuildContext context,
      @required IndexController globalState}) async {}
}
