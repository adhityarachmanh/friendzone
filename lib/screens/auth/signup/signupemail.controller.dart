/*
module  : SIGNUPEMAIL CONTROLLER
creator : arh
os      : linux-gnu
created : Rab Jan 13 13:53:54 WIB 2021
product : ARH
version : v1.0
*/

part of '../../../app.dart';

class SignupemailController with ChangeNotifier {
  final String title = "SignupemailControllerWorks!";
  String _email = "";
  String get email => _email;
  Color get isActiveColor =>
      EmailValidator.validate(email) ? Palette.fzColor : Palette.grayColor;

  void setEmail(String email) {
    _email = email;
    notifyListeners();
  }

  void check(
      {@required BuildContext context,
      @required IndexController globalState}) async {
    final route = Provider.of<RouteFunction>(context, listen: false);
    if (!EmailValidator.validate(email)) return;
    var response = await AuthService.checkEmail(email);
    switch (response.s) {
      case 0:
        RegisterModel registerData = new RegisterModel(email: email);
        Provider.of<IndexController>(context, listen: false)
            .setRegisterData(registerData);
        route.navigateTo(SignuppasswordScreen.routeName);
        break;
      default:
        showDialog<void>(
          context: context,
          barrierDismissible: false, // user must tap button!
          builder: (BuildContext context) {
            return PlatformAlertDialog(
              // title: Text(
              //   "This email is already connected to an account.",
              //   style: MyText.dark(font:MyFont.montserrat).copyWith(fontWeight: FontWeight.w500)
              // ),
              content: SingleChildScrollView(
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                    Text("This email is already",
                        style: MyText.dark(font: MyFont.montserrat).copyWith(
                            fontWeight: FontWeight.bold, fontSize: 16)),
                    SizedBox(
                      height: defaultMargin * 0.5,
                    ),
                    Text("connected to an account.",
                        style: MyText.dark(font: MyFont.montserrat).copyWith(
                            fontWeight: FontWeight.bold, fontSize: 16)),
                    SizedBox(
                      height: defaultMargin * 0.5,
                    ),
                    Text("Do you want to log in instead?",
                        style: MyText.dark(font: MyFont.montserrat).copyWith(
                            fontWeight: FontWeight.w300, fontSize: 12)),
                    SizedBox(
                      height: defaultMargin * 0.6,
                    ),
                    Center(
                      child: Container(
                        margin: EdgeInsets.only(top: defaultMargin * 0.25),
                        child: ButtonRoundedWidget(
                            width: SizeConfig.screenWidth * 0.5,
                            height: 40,
                            color: Palette.fzColor,
                            label: "GO TO LOGIN",
                            onPressed: () {
                              Navigator.of(context).pop(false);
                              route.navigateToAndRemoveUntil(
                                  SigninScreen.routeName);
                            }),
                      ),
                    ),
                    ButtonFlatWidget(
                      label: "CLOSE",
                      onTap: () => Navigator.of(context).pop(false),
                    )
                  ])),
            );
          },
        );
    }
  }

  void onInit(BuildContext context) {
    print("Init SignupemailController");
  }
}
