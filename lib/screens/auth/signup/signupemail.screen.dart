/*
module  : SIGNUPEMAIL SCREEN
creator : arh
os      : linux-gnu
created : Rab Jan 13 13:53:54 WIB 2021
product : ARH
version : v1.0
*/

part of '../../../app.dart';

class SignupemailScreen extends StatefulWidget {
  static final routeName = "/SignupemailScreen";

  @override
  _SignupemailScreenState createState() => _SignupemailScreenState();
}

class _SignupemailScreenState extends State<SignupemailScreen> {
  final TrackingScrollController _trackingScrollController =
      TrackingScrollController();

  @override
  void dispose() {
    _trackingScrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final globalDispatch = Provider.of<IndexController>(context, listen: false);
    final route = Provider.of<RouteFunction>(context, listen: false);
    return WillPopScope(
      onWillPop: () async {
        globalDispatch.setRegisterData(null);
        route.navigateToAndRemoveUntil(WelcomeScreen.routeName);
        return;
      },
      child: Scaffold(
          body: ResponsiveWidget(
        mobile: _SignupemailScreenMobile(
            scrollController: _trackingScrollController),
      )),
    );
  }
}

class _SignupemailScreenMobile extends StatelessWidget {
  final TrackingScrollController scrollController;
  final TextEditingController emailController = TextEditingController(text: "");
  _SignupemailScreenMobile({Key key, @required this.scrollController})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    // Global controller
    final globalState = Provider.of<IndexController>(context);
    final globalDispatch = Provider.of<IndexController>(context, listen: false);
    // Route
    final route = Provider.of<RouteFunction>(context, listen: false);
    final routeParams = route.getParams(context);
    // Websocket
    final websocket = Provider.of<WebsocketFunction>(context, listen: false);
    // Local controller
    final state = Provider.of<SignupemailController>(context);
    final dispatch = Provider.of<SignupemailController>(context, listen: false);
    globalDispatch.setStatusBar(background: Palette.fzColor);
    return CustomScrollView(
      slivers: [
        SliverAppBar(
          iconTheme: IconThemeData(color: Palette.darkColor),
          backgroundColor: Palette.scaffold,
          title: Text("Create Account",
              style: MyText.dark(font: MyFont.montserrat)
                  .copyWith(fontSize: 16, fontWeight: FontWeight.w400)),
          centerTitle: false,
          floating: true,
        ),
        SliverToBoxAdapter(
          child: Container(
            margin: EdgeInsets.symmetric(
                vertical: defaultMargin * 0.8, horizontal: defaultMargin * 0.4),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("What's your email?",
                    style: MyText.dark(font: MyFont.montserrat)
                        .copyWith(fontSize: 24, fontWeight: FontWeight.bold)),
                Container(
                    margin: EdgeInsets.only(
                        top: defaultMargin * 0.40,
                        bottom: defaultMargin * 0.20),
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                      color: Palette.grayColor,
                      borderRadius: BorderRadius.circular(2.5),
                    ),
                    child: TextFormField(
                      initialValue: state.email,
                      onChanged: (value) => dispatch.setEmail(value),
                      autofocus: true,
                      style: TextStyle(color: Palette.lightColor),
                      cursorColor: Palette.lightColor,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                      ),
                    )),
                Text("You'll need to confirm this email later.",
                    style: MyText.dark(font: MyFont.montserrat)
                        .copyWith(fontSize: 12, fontWeight: FontWeight.w300)),
                Center(
                  child: Container(
                    margin: EdgeInsets.only(top: defaultMargin * 0.25),
                    child: ButtonRoundedWidget(
                      width: SizeConfig.screenWidth * 0.35,
                      height: 40,
                      color: dispatch.isActiveColor,
                      label: "NEXT",
                      onPressed: () => dispatch.check(
                        context: context,
                        globalState: globalState,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        )
      ],
    );
  }
}
