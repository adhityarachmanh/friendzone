/*
module  : SIGNUPGENDER SCREEN
creator : adhityarachmanh
os      : darwin20
created : Sun Jan 17 00:01:35 WIB 2021
product : ARH
version : v1.0
*/

part of '../../../app.dart';

class SignupgenderScreen extends StatefulWidget {
  static final routeName = "/SignupgenderScreen";

  @override
  _SignupgenderScreenState createState() => _SignupgenderScreenState();
}

class _SignupgenderScreenState extends State<SignupgenderScreen> {
  final TrackingScrollController _trackingScrollController = TrackingScrollController();

  @override
  void dispose() {
    _trackingScrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
    body: ResponsiveWidget(
      mobile:_SignupgenderScreenMobile(scrollController: _trackingScrollController),
    ));
  }
}

class _SignupgenderScreenMobile extends StatelessWidget {
  final TrackingScrollController scrollController;

  const _SignupgenderScreenMobile({Key key, @required this.scrollController})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    // Global controller
    final globalState = Provider.of<IndexController>(context);
    final globalDispatch = Provider.of<IndexController>(context, listen: false);
    // Route
    final route = Provider.of<RouteFunction>(context, listen: false);
    final routeParams = route.getParams(context);
    // Websocket
    final websocket = Provider.of<WebsocketFunction>(context, listen: false);
    // Local controller
    final state = Provider.of<SignupgenderController>(context);
    final dispatch = Provider.of<SignupgenderController>(context, listen: false);
    globalDispatch.setStatusBar(background: Palette.fzColor);
    return CustomScrollView(
      slivers: [
        SliverToBoxAdapter(
          child: Container(
            height: SizeConfig.screenHeight,
            child: Center(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("SignupgenderMobileScreen!",style: MyText.dark(font: MyFont.montserrat).copyWith(fontSize: 25)),
                SizedBox(height: defaultMargin),
                Text(state.title, style: MyText.dark(font: MyFont.montserrat).copyWith(fontSize: 25)),
              ],
            )),
          ),
        )
      ],
    );
  }
}
