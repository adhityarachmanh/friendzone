/*
module  : SPLASH CONTROLLER
creator : arh
os      : linux-gnu
created : Rab Jan 13 13:32:20 WIB 2021
product : ARH
version : v1.0
*/

part of '../app.dart';

class SplashController with ChangeNotifier {
    final String title = "SplashControllerWorks!";
   
    void onInit(BuildContext context){
    	print("Init SplashController");
    }
}
