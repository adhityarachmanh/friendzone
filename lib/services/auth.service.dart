/*
module  : AUTH SERVICE
creator : adhityarachmanh
os      : darwin19
created : Thu Dec 17 20:21:01 WIB 2020
product : ARH
version : v1.0
*/

part of '../app.dart';

class AuthService {
  static FirebaseAuth _auth = FirebaseAuth.instance;
  static CollectionReference userCollection =
      FirebaseFirestore.instance.collection('users');

  static Future<ResponseAPI> signin(String eu, String password) async {
    try {
      String key = "";
      if (!emailRegEx.hasMatch(eu))
        key = "Username";
      else
        key = "Email";

      // find user data with email or username
      var response =
          await userCollection.where(key.toLowerCase(), isEqualTo: eu).get();
      if (response.docs.length == 0) {
        return ResponseAPI(s: 1, msg: "No user found for that $key.");
      }
      UserModel user = UserModel.fromJson(
          {"uid": response.docs.first.id, ...response.docs.first.data()});

      // do login to firebase auth
      UserCredential userCredential = await _auth.signInWithEmailAndPassword(
          email: user.email, password: password);

      // if success set new token to data user
      var token = global.createToken();
      FirebaseFirestore.instance.runTransaction((transaction) async {
        transaction.update(
          userCollection.doc(userCredential.user.uid),
          {"token": token},
        );
      });
      // set token to localstorage
      await global.setToken(token);
      return ResponseAPI(s: 0, data: user, msg: "SignIn Success.");
    } on FirebaseAuthException catch (e) {
      String error;
      switch (e.code) {
        case 'wrong-password':
          error = 'Wrong password provided for that user.';
          break;
        default:
          error = e.message;
      }
      return ResponseAPI(s: 1, msg: error);
    }
  }

  static Future<ResponseAPI> checkEmail(String email) async {
    try {
      var response =
          await userCollection.where("email", isEqualTo: email).get();
      if (response.docs.length != 0) {
        return ResponseAPI(s: 1);
      }
      return ResponseAPI(s: 0);
    } catch (e) {
      return ResponseAPI(s: 1);
    }
  }

  static Future<ResponseAPI> authState(String token) async {
    try {
      var response =
          await userCollection.where("token", isEqualTo: token).get();
      if (response.docs.length == 0) {
        return ResponseAPI(s: 1, msg: "No user found for that token");
      }
      UserModel user = UserModel.fromJson(
          {"uid": response.docs.first.id, ...response.docs.first.data()});
      return ResponseAPI(s: 0, data: user);
    } catch (e) {
      return ResponseAPI(s: 1, msg: "Server Error.");
    }
  }

  static Future<void> signout() async {
    await global.removeToken();
    await _auth.signOut();
  }

  static Stream<User> get authStream => _auth.authStateChanges();
}
