/*
module  : USER MODEL
creator : adhityarachmanh
os      : darwin19
created : Thu Dec 17 20:21:01 WIB 2020
product : ARH
version : v1.0
*/

part of '../app.dart';

class UserModel extends Equatable {
  final String uid;
  final String username;
  final String email;

  UserModel(this.uid, {this.username, this.email});

  factory UserModel.fromJson(Map<String, dynamic> json) {
    return UserModel(json['uid'],
        username: json['username'], email: json['email']);
  }

  Map<String, dynamic> toJson() {
    return {
      "uid": this.uid ?? "",
      "email": this.email ?? "",
      "username": this.username ?? "",
    };
  }

  UserModel copyWith() => UserModel(this.uid);

  @override
  List<Object> get props => [];
}
