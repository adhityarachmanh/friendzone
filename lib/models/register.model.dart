/*
module  : REGISTER MODEL
creator : adhityarachmanh
os      : darwin20
created : Sat Jan 16 21:31:59 WIB 2021
product : ARH
version : v1.0
*/

part of '../app.dart';

class RegisterModel extends Equatable {
  final String email;
  RegisterModel({@required this.email});

  factory RegisterModel.fromJson(Map<String, dynamic> json) {
    return RegisterModel(email:json['email']);
  }

  Map<String, dynamic> toJson() {
    return {"email": this.email??""};
  }

  RegisterModel copyWith({String email}) => RegisterModel(email:email);

  @override
  List<Object> get props => [email];
}
