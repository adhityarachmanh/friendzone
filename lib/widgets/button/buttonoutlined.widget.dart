/*
module  : BUTTONOUTLINED WIDGET
creator : adhityarachmanh
os      : darwin19
created : Wed Jan  6 07:27:41 WIB 2021
product : ARH
version : v1.0
*/

part of '../../app.dart';

class ButtonOutlinedWidget extends StatelessWidget {
  final Function onPressed;
  final Widget icon;
  final Color borderColor;
  final String label;
  final double width;
  final double height;
  ButtonOutlinedWidget(
      {@required this.onPressed,
      @required this.label,
      this.icon = const SizedBox(),
      this.borderColor = Colors.blue,
      this.width = double.infinity,
      this.height =  25 * 1.8

      });
  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(
          top: defaultMargin * 0.3,
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(25),
          border: Border.all(
              color: borderColor, width: 1),
        ),
        width: width,
        height: height,
        child: ClipRRect(
            borderRadius: BorderRadius.circular(25),
            child: FlatButton(
                child: Row(
                  children: [
                    icon,
                    icon != null ? Spacer() : SizedBox(),
                    Center(
                      child: Text(
                        label,
                        style: MyText.primary(font: MyFont.montserrat)
                            .copyWith(fontWeight: FontWeight.w600,fontSize:14),
                      ),
                    ),
                    icon != null ? Spacer() : SizedBox(),
                  ],
                ),
                color: Colors.transparent,
                onPressed: onPressed)));
  }
}
