/*
module  : BUTTONFLAT WIDGET
creator : adhityarachmanh
os      : darwin19
created : Wed Jan  6 08:24:43 WIB 2021
product : ARH
version : v1.0
*/

part of '../../app.dart';

class ButtonFlatWidget extends StatelessWidget {
  final Function onTap;
  final String label;
  ButtonFlatWidget({
    @required this.label,
    @required this.onTap,
    });
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        top: defaultMargin * 0.8,
      ),
      child: Center(
        child: InkWell(
          onTap: onTap,
          child: Text(
            label,
            style: MyText.primary(font: MyFont.montserrat)
                .copyWith(fontWeight: FontWeight.w600,fontSize:14),
          ),
        ),
      ),
    );
  }
}
