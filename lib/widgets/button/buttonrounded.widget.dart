/*
module  : BUTTONROUNDED WIDGET
creator : adhityarachmanh
os      : darwin19
created : Wed Jan  6 07:27:35 WIB 2021
product : ARH
version : v1.0
*/

part of '../../app.dart';

class ButtonRoundedWidget extends StatelessWidget {
  final Function onPressed;
  final Color color;
  final String label;
  final double width;
  final double height;
  const ButtonRoundedWidget({
    @required this.onPressed,
    @required this.label,
    this.color = Colors.blue,
    this.width =  double.infinity,
    this.height =  25 * 1.8

  });
  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(
          top: defaultMargin * 0.60,
        ),
        width: width,
        height: height,
        child: ClipRRect(
            borderRadius: BorderRadius.circular(25),
            child: RaisedButton(
                child: Text(
                  label,
                  style: MyText.light(font: MyFont.montserrat)
                      .copyWith(fontWeight: FontWeight.w600,letterSpacing: 0.5,fontSize:14)
                ),
                color: color,
                onPressed: onPressed)));
  }
}
