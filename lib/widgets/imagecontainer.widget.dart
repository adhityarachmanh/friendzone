/*
module  : IMAGECONTAINER WIDGET
creator : adhityarachmanh
os      : darwin20
created : Sat Jan 16 21:25:23 WIB 2021
product : ARH
version : v1.0
*/

part of '../app.dart';

class ImageContainerWidget extends StatelessWidget {
  final String path;
  final double width;
  final double height;

  ImageContainerWidget(this.path, {this.width = 100.0, this.height = 100.0});
  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      child: Image.asset(path),
    );
  }
}
