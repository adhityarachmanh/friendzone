/*
module  : MAIN
creator : adhityarachmanh
os      : darwin19
created : Thu Dec 17 20:21:01 WIB 2020
product : ARH
version : v1.0
*/

import 'dart:async';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'app.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  runApp(MultiProvider(
    providers: [
      // GLobal Provider
      ChangeNotifierProvider.value(value: RouteFunction()),
      ChangeNotifierProvider.value(value: WebsocketFunction()),
      ChangeNotifierProvider.value(value: IndexController()),
    ],
    child: Main(),
  ));
}

class Main extends StatelessWidget {
  static FirebaseAnalytics analytics = FirebaseAnalytics();
  static FirebaseAnalyticsObserver observer =
      FirebaseAnalyticsObserver(analytics: analytics);

  @override
  Widget build(BuildContext context) {
    final globalDispatch = Provider.of<IndexController>(context, listen: false);
    globalDispatch.onInit(context);
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
            primaryColor: Palette.primaryColor,
            visualDensity: VisualDensity.adaptivePlatformDensity,
            scaffoldBackgroundColor: Palette.scaffold,
            pageTransitionsTheme: PageTransitionsTheme(builders: {
              TargetPlatform.iOS: FadeUpwardsPageTransitionsBuilder(),
              TargetPlatform.android: FadeUpwardsPageTransitionsBuilder(),
            })),
        navigatorObservers: <NavigatorObserver>[observer],
        navigatorKey: Provider.of<RouteFunction>(context).navigationKey,
        initialRoute: SplashScreen.routeName,
        routes: routes);
  }
}
