/*
module  : APP
creator : adhityarachmanh
os      : darwin19
created : Thu Dec 17 20:21:01 WIB 2020
product : ARH
version : v1.0
*/
import 'dart:async';
import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:email_validator/email_validator.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/services.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:path/path.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'dart:math';
import 'dart:math' show cos, sqrt, asin;
import 'package:uuid/uuid.dart';
import 'package:web_socket_channel/io.dart';
part 'config.dart';
part 'index.dart';
part 'route.dart';
part 'styling.dart';
part 'shr/websocketFunction.dart';
part 'shr/routeFunction.dart';
part 'shr/global.dart';
part 'shr/api.dart';
part 'extensions/hex.extension.dart';
part 'models/user.model.dart';
part 'services/auth.service.dart';
part 'screens/splash.screen.dart';
part 'screens/splash.controller.dart';
part 'widgets/safearea.widget.dart';
part 'widgets/responsive.widget.dart';
part 'widgets/button/buttonrounded.widget.dart';
part 'widgets/button/buttonoutlined.widget.dart';
part 'widgets/button/buttonflat.widget.dart';
part 'screens/dashboard.screen.dart';
part 'screens/dashboard.controller.dart';
part 'screens/welcome.screen.dart';
part 'screens/welcome.controller.dart';
part 'screens/auth/signin.screen.dart';
part 'screens/auth/signin.controller.dart';
part 'screens/auth/signup/signupemail.screen.dart';
part 'screens/auth/signup/signupemail.controller.dart';
part 'widgets/imagecontainer.widget.dart';
part 'models/register.model.dart';
part 'screens/auth/signup/signuppassword.screen.dart';
part 'screens/auth/signup/signuppassword.controller.dart';
part 'screens/auth/signup/signupbirthday.screen.dart';
part 'screens/auth/signup/signupbirthday.controller.dart';
part 'screens/auth/signup/signupgender.screen.dart';
part 'screens/auth/signup/signupgender.controller.dart';
part 'screens/auth/signup/signupname.screen.dart';
part 'screens/auth/signup/signupname.controller.dart';
