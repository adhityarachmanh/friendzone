/*
module  : INDEX
creator : adhityarachmanh
os      : darwin19
created : Thu Dec 17 20:21:01 WIB 2020
product : ARH
version : v1.0
*/

part of 'app.dart';

class IndexController with ChangeNotifier {
  UserModel _user;
  RegisterModel _registerData;
  RegisterModel get registerData => _registerData;
  UserModel get user => _user;
  
  void setRegisterData(RegisterModel payload) {
    _registerData = payload;
    notifyListeners();
  }

  void userInfo(UserModel payload) {
    _user = payload;
    notifyListeners();
  }

  void setStatusBar(
      {@required Color background, Brightness icon = Brightness.light}) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: background, statusBarIconBrightness: icon));
  }

  Future<void> autoLogin({@required BuildContext context}) async {
    final route = Provider.of<RouteFunction>(context, listen: false);
    final token = await global.getToken();
    if (token != null) {
      var response = await AuthService.authState(token);
      switch (response.s) {
        case 0:
          UserModel user = response.data;
          userInfo(user);
          route.navigateTo(DashboardScreen.routeName);
          break;
        default:
      }
    } else {
      Timer(Duration(seconds: 3),
          () => route.navigateToAndRemoveUntil(WelcomeScreen.routeName));
    }
  }

  Future<void> signOut({@required BuildContext context}) async {
    final route = Provider.of<RouteFunction>(context, listen: false);
    await AuthService.signout();
    userInfo(null);
    route.navigateToAndRemoveUntil(WelcomeScreen.routeName);
  }

  onInit(BuildContext context) {
    autoLogin(context: context);
  }
}
