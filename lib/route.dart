/*
module  : ROUTE
creator : adhityarachmanh
os      : darwin19
created : Thu Dec 17 20:21:01 WIB 2020
product : ARH
version : v1.0
*/
part of 'app.dart';
Map<String, Widget Function(BuildContext)> routes = {
	SplashScreen.routeName: (ctx) => ChangeNotifierProvider.value(value: SplashController(), child: SplashScreen()),
	DashboardScreen.routeName: (ctx) => ChangeNotifierProvider.value(value: DashboardController(), child: DashboardScreen()),
	WelcomeScreen.routeName: (ctx) => ChangeNotifierProvider.value(value: WelcomeController(), child: WelcomeScreen()),
	SigninScreen.routeName: (ctx) => ChangeNotifierProvider.value(value: SigninController(), child: SigninScreen()),
	SignupemailScreen.routeName: (ctx) => ChangeNotifierProvider.value(value: SignupemailController(), child: SignupemailScreen()),
	SignuppasswordScreen.routeName: (ctx) => ChangeNotifierProvider.value(value: SignuppasswordController(), child: SignuppasswordScreen()),
	SignupbirthdayScreen.routeName: (ctx) => ChangeNotifierProvider.value(value: SignupbirthdayController(), child: SignupbirthdayScreen()),
	SignupgenderScreen.routeName: (ctx) => ChangeNotifierProvider.value(value: SignupgenderController(), child: SignupgenderScreen()),
	SignupnameScreen.routeName: (ctx) => ChangeNotifierProvider.value(value: SignupnameController(), child: SignupnameScreen()),
};
